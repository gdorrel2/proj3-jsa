# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

This implementation  fills in times as the input fields using Ajax and Flask:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* the logic in acp_times.py is based on the algorithm given above. 
based off the brevet calucaltion rules given here https://rusa.org/pages/acp-brevet-control-times-calculator) I used the row that first defined what min and max speed for edge cases that are defined twice ie for brevet distance 200 i used 34 instead of 32 based on the origininal calcualtor
I also made the last brevet close time for a 200 km 13.5 hours after the start based on randonneurs rules

## Testing

A suite of nose test cases is also included

there is also a run.sh file that automatically builds the contianer and runs the test cases




